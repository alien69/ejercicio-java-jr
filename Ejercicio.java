package ejercicio;

import java.util.ArrayList;
import java.util.*;

import java.util.Collections;
import java.util.Comparator;

public class Ejercicio
{
    
    public String marca;
    public String modelo;
    public String pc;
    public double precio;
    public String letra;
    public String coma;
    
    public Ejercicio(String _letra, String _marca, String _modelo, String _pc, double _precio, String _coma)
    {
        letra = _letra;
        
        marca = _marca;
        
        modelo = _modelo;
        
        pc = _pc;
        
        precio = _precio;    
        
        coma = _coma; 
    }
    
    public double Precio()
    {
        return precio;
    }
    
    public static void main(String[] args)
    {
        
        ArrayList<Ejercicio> inventario = new ArrayList<Ejercicio>();
        inventario.add(new Ejercicio("2", "Peugeot", "206", "Puertas: 4", 200.000, ",00"));
        inventario.add(new Ejercicio("T", "Honda", "Titan", "Cilindrada: 125c", 60.000, ",00"));
        inventario.add(new Ejercicio("2", "Peugeot", "208", "Puertas: 5", 250.000, ",00"));
        inventario.add(new Ejercicio("Y", "Yamaha", "YBR", "Cilindrada: 160c", 80.500, ",50"));
        
        for(Ejercicio e:inventario)
        {
            System.out.format("Marca: " + e.marca + " // Modelo: " + e.modelo + " // " + e.pc + " // %.3f", e.precio);
            System.out.print(e.coma + "\n");
        }
        
        System.out.println("========================================================================");
            
                for(Ejercicio in:inventario)
                {
                    if( in.precio > 200.000)
                    {
                        System.out.printf("Vehiculo mas caro: " + in.marca + " " + in.modelo + "\n");
                        for(Ejercicio inv:inventario)
                        {
                            if( inv.precio < 70.000)
                            {
                                System.out.printf("Vehiculo mas barato: " + inv.marca + " " + inv.modelo + "\n");
                            }                            
                        }
                    }
            } 
                

        for(Ejercicio i:inventario)
        {
           if(i.letra == "Y")
           {
               System.out.format("Vehículo que contiene en el modelo la letra ‘Y’: " + i.marca + " " + i.modelo + " %.3f", i.precio);
               System.out.print(i.coma + "\n");
           }
        }
        
        System.out.println("========================================================================");
        
        Collections.sort(inventario, new Comparator<Ejercicio>() {
            @Override
            public int compare(Ejercicio p1, Ejercicio p2) {
                if( p1.Precio() < p2.Precio())
                {
                    
                    return 1;
                }
                if( p1.Precio() > p2.Precio())
                {
                    return -1;
                }
                return 0;
            }
        });
        
        for(Ejercicio p:inventario)
        {
            System.out.println(p.marca + " " + p.modelo);
        }
        
    }
}
